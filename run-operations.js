// importer le fichier operations.js
const operations = require('./operations.js');


let result = 0;

console.log('####### START #######');

for (let index = 0; index < 1000000; index++) {
    result = operations.add(result, index);
    console.log(result);
}

console.log('####### END #######');